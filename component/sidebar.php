<?php if(isset($method)):?>
<div class='content--sidebar'>

			<div class='content--sidebar__widget'>

				<div class='widget widget--agenda'>

					<div class='widget__title'>
						<h3>
							<i class='fa fa-calendar-o widget__icon'></i><a href='#'>AGENDA <i class='fa fa-arrow-circle-right'></i></a>
						</h3>
					</div>


					<div class='widget__content'>
						<div class='agenda__content'>

							<div class='agenda__content__section'>

								<div class='agenda__content__section__title'>
									<h4>WALIKOTA</h4>
								</div>
								
								<div class='agenda__content__section__list'>

									<ul class='agenda__list'>
										
										<?php
										$agenda = $event->getEvent(0,10);
										foreach($agenda as $agenda)
										{
											
											echo "
										
											<li>
												<a href='".ROOT."agenda/".$agenda['id']."/".$agenda['link']."'><i class='fa fa-map-marker'></i></a><p><a href='".ROOT."agenda/".$agenda['id']."/".$agenda['link']."'>".$agenda['judul']."</a></p><span class='agenda__list__date'>".date("d/m",strtotime($agenda['tanggal_mulai']))."</span>
											</li>
											";
										}
										?>
											
            								
											
            								
            							</ul>

								</div>

							</div>

						</div>

					</div>

				</div> 







				<div class='widget widget--announcement'>

					<div class='widget__title'>
						<h3>
							<i class='fa fa-globe widget__icon'></i><a href='#'>Berita Terbaru</a>
						</h3>
					</div>


					<div class='widget__content'>

						<ul class='widget__content--announcement'>

							<?php 
							
								$side_berita = $artikel->getArtikel(0,4);
								foreach($side_berita as $side_berita){
							echo "
								
								<li>
									<p class='widget__content--announcement__date'>".$libs->tgl_indo($side_berita['tanggal'])."</p>
									<p class='widget__content--announcement__title'><a href='".ROOT.'berita/'.$side_berita['id'].'/'.$side_berita['link']."'>".$side_berita['judul']."</a></p>
								</li>

								";
								}
							?>
						</ul>

					</div>

				</div>




<!--

				<div class='widget widget--facebook'>

					<div class='widget__title'>
						<h3>
							<i class='fa fa-facebook widget__icon'></i><a href='#'>IKUTI KAMI DI FACEBOOK</a>
						</h3>
					</div>


					<div class='widget__content'>

						<div class='facebook__content'>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultricies orci et sapien pharetra.. </p>

							<button class='button--square button--white' href='#'>FOLLOW</button>
						</div>

					</div>

				</div>

 -->




				<div class='widget widget--twitter'>

					<div class='widget__title'>
						<h3>
							<i class='fa fa-twitter widget__icon'></i><a href='#'>IKUTI KAMI DI SOSIAL MEDIA</a>
						</h3>
					</div>


					<div class='widget__content'>

						<div class='facebook__content'>
						
							<a class="  button--square button--white twitter-follow-button"
								  href="https://twitter.com/<?php $a = $home->getPengaturan(); echo trim($a['tw']); ?>"
								  data-show-count="true"
								  data-lang="en"
								  data-width="300px"
								  data-align="left"

								  >
								
							</a><br/>
									

							<div id="fb-root"></div>

								<div class="fb-like-box" data-href="https://www.facebook.com/<?php $ffb=$home->getPengaturan(); echo trim($ffb['fb']);?>" data-header="false"  data-width="330" data-connections="5" data-height="212"  data-colorscheme="dark" data-show-faces="true" data-stream="false" data-show-border="false"></div>
							
							<!--
							<button class='button--square button--white' href='#'>FOLLOW</button>
							-->
						</div>

					</div>

				</div>


			</div>

		</div>


<?php endif;?>