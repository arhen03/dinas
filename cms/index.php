<?php
session_start();
// var_dump($_GET);
include('libs/path.php');
require 'model/class.php';
if(empty($_SESSION['login'])){
	header('location:'.URL.'login');
}
	// include'config.php';

$url = isset($_GET['p']) ? $_GET['p'] : null;
$url = rtrim($url, '/');
$url = filter_var($url, FILTER_SANITIZE_URL);
$url = explode('/', $url);      // memecah URL menjadi variabel dimana var pertama adalah model dan yang lain adalah 

#config dasar
$model 		= $url[0];
$method 	= !empty($url[1])?$url[1]:'';
$parameter 	= !empty($url[2])?$url[2]:null;

// $parameter = filter_var($parameter,FILTER_SANITIZE_STRING);
$method = filter_var($method,FILTER_SANITIZE_STRING);
// var_dump($url);


?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang='en' class='ie8'> <![endif]-->
<!--[if IE 9]> <html lang='en' class='ie9'> <![endif]-->
<!--[if !IE]><!--> <html lang='en'> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset='utf-8' />
   <title>Halaman Admin</title>
   <meta content='width=device-width, initial-scale=1.0' name='viewport' />
   <meta content='' name='description' />
   <?php 
   // ini untuk cssnya
   echo "
	   <link href='".URL."assets/bootstrap/css/bootstrap.min.css' rel='stylesheet' />
	   <link href='".URL."assets/bootstrap/css/bootstrap-responsive.min.css' rel='stylesheet' />
	   <link href='".URL."assets/bootstrap/css/bootstrap-fileupload.css' rel='stylesheet' />
	   <link href='".URL."assets/font-awesome/css/font-awesome.css' rel='stylesheet' />
	   <link href='".URL."css/style.css' rel='stylesheet' />
	   <link href='".URL."css/style-responsive.css' rel='stylesheet' />
	   <link href='".URL."css/style-default.css' rel='stylesheet' id='style_color' />
	   <link href='".URL."assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css' rel='stylesheet' />
	   <link href='".URL."assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css' rel='stylesheet' type='text/css' media='screen'/>
	   <link rel='stylesheet' type='text/css' href='".URL."assets/nestable/jquery.nestable.css' />
	   <link rel='stylesheet' type='text/css' href='".URL."css/jquery-gmaps-latlon-picker.css'/>
	   <link rel='stylesheet' type='text/css' href='".URL."assets/bootstrap-datepicker/css/datepicker.css' />
   
		
   ";
   
   ?>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class='fixed-top'>
   <!-- BEGIN HEADER -->
   <div id='header' class='navbar navbar-inverse navbar-fixed-top'>
       <!-- BEGIN TOP NAVIGATION BAR -->
       <div class='navbar-inner'>
           <div class='container-fluid'>
               <!--BEGIN SIDEBAR TOGGLE-->
               <div class='sidebar-toggle-box hidden-phone'>
                   <div class='icon-reorder tooltips' data-placement='right' data-original-title='Toggle Navigation'></div>
               </div>
               <!--END SIDEBAR TOGGLE-->
               <!-- BEGIN LOGO -->
				<?php 
				echo "
				<a class='brand' href='".URL."' style='width:400px; color:white;'>
                   <b>Halaman Admin</b>
               </a>
				
				";
				
				?>
				   <!--<img src='img/logo.png' alt='Logo' />->
               <!-- END LOGO -->
               <!-- BEGIN RESPONSIVE MENU TOGGLER -->
               <a class='btn btn-navbar collapsed' id='main_menu_trigger' data-toggle='collapse' data-target='.nav-collapse'>
                   <span class='icon-bar'></span>
                   <span class='icon-bar'></span>
                   <span class='icon-bar'></span>
                   <span class='arrow'></span>
               </a>
               <!-- END RESPONSIVE MENU TOGGLER -->
               <div id='top_menu' class='nav notify-row'>
                   <!-- BEGIN NOTIFICATION -->
                   
               </div>
               <!-- END  NOTIFICATION -->
               <div class='top-nav '>
                   <ul class='nav pull-right top-menu' >
                       <!-- BEGIN SUPPORT -->
                       <!-- END SUPPORT -->
                       <!-- BEGIN USER LOGIN DROPDOWN -->
                       <li class='dropdown'>
                           <a href='#' class='dropdown-toggle' data-toggle='dropdown'>
                               <?php 
							   
							   echo "<span class='username'>".$_SESSION['username']."</span>";
							   
							   ?>
                               
                               <b class='caret'></b>
                           </a>
                           <ul class='dropdown-menu extended logout'>
                               <?php 
							   echo "
							   <li><a  target='_blank' href='".ROOT."'><i class='icon-eye-open'></i> Halaman Depan</a></li>
							   <li><a href='".URL."logout'><i class='icon-key'></i> Log Out</a></li>
							   ";
							   ?>
                           </ul>
                       </li>
                       <!-- END USER LOGIN DROPDOWN -->
                   </ul>
                   <!-- END TOP NAVIGATION MENU -->
               </div>
           </div>
       </div>
       <!-- END TOP NAVIGATION BAR -->
   </div>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id='container' class='row-fluid'>
      <!-- BEGIN SIDEBAR -->
      <div class='sidebar-scroll'>
        <div id='sidebar' class='nav-collapse collapse'>

         <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
         <div class='navbar-inverse'>
            <form class='navbar-search visible-phone'>
               <input type='text' class='search-query' placeholder='Search' />
            </form>
         </div>
         <!-- END RESPONSIVE QUICK SEARCH FORM -->
         <!-- BEGIN SIDEBAR MENU -->
          <ul class='sidebar-menu'>
			  <?php
			    echo ($model == '')? "<li class='sub-menu active'>":"<li class='sub-menu '>";
              echo "
			  
                  <a class='' href='".URL."'>
                      <i class='icon-dashboard'></i>
                      <span>Dashboard</span>
                  </a>
				</li>
			  ";
			   
			  echo ($model == 'aspirasi')? "<li class='sub-menu active'>":"<li class='sub-menu '>";
			  echo "
                  <a class='' href='".URL."aspirasi'>
                      <i class=' icon-warning-sign'></i>
                      <span>Aspirasi</span>
                  </a>
				</li>
			  ";
			  
			  echo ($model == 'berita')? "<li class='sub-menu active'>":"<li class='sub-menu '>";
			  echo "
                  <a class='' href='".URL."berita'>
                      <i class='icon-globe'></i>
                      <span>Berita</span>
                  </a>
				</li>
			  ";
			  
			  echo ($model == 'event')? "<li class='sub-menu active'>":"<li class='sub-menu '>";
			  echo "
                  <a class='' href='".URL."event'>
                      <i class='icon-th-list'></i>
                      <span>Agenda</span>
                  </a>
				</li>
			  ";
			  
			  echo ($model == 'pengumuman')? "<li class='sub-menu active'>":"<li class='sub-menu '>";
			  echo "
                  <a class='' href='".URL."pengumuman'>
                      <i class='icon-bullhorn'></i>
                      <span>Pengumuman</span>
                  </a>
				</li>
			  ";
			  
			  echo ($model == 'halaman' or $model == 'manajemen_halaman')?"<li class='sub-menu active'>":"<li class='sub-menu'>";
              echo 
				 "<a href='javascript:;' class=''>
                      <i class='icon-folder-open'></i>
                      <span>Halaman</span>
                      <span class='arrow'></span>
                  </a>";
				  
               echo "
				  <ul class='sub'>";
               echo   ($model=='halaman')?"<li class='active'><a  href='".URL."halaman'>Halaman</a></li>"
										  :
										  "<li class=''><a  href='".URL."halaman'>Halaman</a></li>";
               echo   ($model =='manajemen_halaman')?"<li class='active'>
														<a  href='".URL."manajemen_halaman'>Manajemen Halaman</a>
													  </li>"
													  :
													  "<li><a class='' href='".URL."manajemen_halaman'>Manajemen Halaman</a>
													  </li>";
			   echo "
                  </ul>
              </li>";
			 
			  echo ($model == 'galeri')? "<li class='sub-menu active'>":"<li class='sub-menu '>";
			  echo "
                  <a class='' href='".URL."galeri'>
                      <i class='icon-picture'></i>
                      <span>Galeri</span>
                  </a>
				</li>
			  ";
			  
			  echo ($model == 'struktur_organisasi')? "<li class='sub-menu active'>":"<li class='sub-menu '>";
			  echo "
                  <a class='' href='".URL."struktur_organisasi'>
                      <i class='icon-user'></i>
                      <span>Struktur Organisasi</span>
                  </a>
				</li>
			  ";
			  
			  echo ($model == 'pengaturan')? "<li class='sub-menu active'>":"<li class='sub-menu '>";
			  echo "
                  <a class='' href='".URL."pengaturan'>
                      <i class='icon-cogs'></i>
                      <span>Pengaturan</span>
                  </a>
				</li>
			  ";
			  
			  echo ($model == 'users')? "<li class='sub-menu active'>":"<li class='sub-menu '>";
			  echo "
                  <a class='' href='".URL."users'>
                      <i class='icon-group'></i>
                      <span>Users</span>
                  </a>
				</li>
			  ";
			  
			  ?>
              
          </ul>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id='main-content'>
         <!-- BEGIN PAGE CONTAINER-->
         <div class='container-fluid'>
            <!-- BEGIN PAGE HEADER-->   
            <div class='row-fluid'>
               <div class='span12'></div>
            </div>
            <!-- END PAGE HEADER-->
			 <?php
			switch($model){ // pilih model
				default:
					include "controller/home_control/home.php";
				break;
				case "berita":
					include "controller/artikel_control/artikel.php";
				break;
				case 'halaman':	
					include "controller/halaman_control/halaman.php";		
				break;
				case 'users':
				if($_SESSION['level']=='admin'){
					include "controller/users_control/users.php";
				}
				break;
				case 'event':
					include "controller/event_control/event.php";
				break;
				case 'pengumuman':
					include "controller/pengumuman_control/pengumuman.php";
				break;
				case 'galeri':
					include "controller/galeri_control/galeri.php";
				break;
				case 'pengaturan':
					include "controller/pengaturan_control/pengaturan.php";
				break;
				case 'aspirasi':
					include "controller/aspirasi_control/aspirasi.php";
				break;
				case 'manajemen_halaman':
					include "controller/manajemen_halaman_control/manajemen_halaman.php";
				break;
				case 'struktur_organisasi':
					include "controller/struktur_organisasi_control/struktur_organisasi.php";
				break;
				case 'logout':
					include "controller/login_control/logout.php";
				break;
				
			}
			
			
			if(empty($_SESSION['login'])){
				// header("location:".URL."");
				// echo "<script> document.location.href='".URL."login'</script>";
			// return false;
				}
			
			
			echo "
			
			";
			
			?>
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id='footer'>
       <?php 
	   echo "".date('Y')." &copy; Pemerintah Kota Makassar.";
	   ?>
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src='<?php echo URL;?>js/jquery-1.8.3.min.js'></script>
   <script src='<?php echo URL;?>js/jquery.nicescroll.js' type='text/javascript'></script>
   <script type='<?php echo URL;?>text/javascript' src='assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js'></script>
   <script type='<?php echo URL;?>text/javascript' src='assets/jquery-slimscroll/jquery.slimscroll.min.js'></script>
   <script src='<?php echo URL;?>assets/fullcalendar/fullcalendar/fullcalendar.min.js'></script>
   <script src='<?php echo URL;?>assets/bootstrap/js/bootstrap.min.js'></script>
   <script type='text/javascript' src='<?php echo URL;?>js/jquery-gmaps-latlon-picker.js'></script>
   <script src='http://maps.googleapis.com/maps/api/js?sensor=false'></script>
   
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src='js/excanvas.js'></script>
   <script src='js/respond.js'></script>
   <![endif]

   <script src='<?php echo URL;?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js' type='text/javascript'></script>
   <script src='<?php echo URL;?>assets/chart-master/Chart.js'></script>
   
   
   <script src='<?php echo URL;?>js/easy-pie-chart.js'></script>
   <script src='<?php echo URL;?>js/sparkline-chart.js'></script>
   <script src='<?php echo URL;?>js/home-page-calender.js'></script>
   <script src='<?php echo URL;?>js/home-chartjs.js'></script>-->
   <script src='<?php echo URL;?>js/jquery.sparkline.js' type='text/javascript'></script>

   <script src='<?php echo URL;?>js/jquery.scrollTo.min.js'></script>
   <script src='<?php echo URL;?>js/common-scripts.js'></script>

   <!--common script for all pages-->

   <!--script for this page only-->

	<script src="<?php echo URL;?>js/jquery-1.7.2.min.js"></script> 
	<script src="<?php echo URL;?>js/bootstrap.js"></script> 
	<script src="<?php echo URL;?>js/base.js"></script> 

   <!--script for this page only-->
   
    <script type="text/javascript" src="<?php echo URL; ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?php echo URL; ?>js/tinymce2/tinymce.min.js"></script>
	<script type="text/javascript">
	
	
	$(function() {
		
		$('#dp1').datepicker({
			format: 'dd-mm-yyyy'
		});
		$('#dp2').datepicker({
			format: 'dd-mm-yyyy'
		});
      	
		$("#uploadFile").on("change", function()
		{
			var files = !!this.files ? this.files : [];
			if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
			if (/^image/.test( files[0].type)){ // only image file
				var reader = new FileReader(); // instance of the FileReader
				reader.readAsDataURL(files[0]); // read the local file
	 
				reader.onloadend = function(){ // set image data as background of div
					// $("#imagePreview").css("background-image", "url("+this.result+")");
					$("#imagePreview").html("<img src='"+this.result+"' style='width:900px; height:300px;'/>");
					
				}
			}
			
			//validasi
			if(files[0].type != 'image/png'){
				if(files[0].type != 'image/jpeg'){
				alert('format gambar tidak sesuai');
				}
			}
			if((files[0].size/1024) >= 1029){
				alert('file melebihi 1 MB');
				return false;
			}
			
		});
	
	});

	// tinymce.init({

		// selector: "textarea",
		// plugins: [
			 // "advlist autolink link  lists charmap print preview hr anchor pagebreak spellchecker",
			 // "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime  nonbreaking",
			 // "save table contextmenu directionality emoticons template paste textcolor "
	   // ],
	   // image_advtab: true,
	   
	 
	   // toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages | print preview media fullpage | forecolor backcolor emoticons" ,
		
		// relative_urls: false
	 // });
	 

	</script>

	<script type="text/javascript">
	$(document).ready(function(){
	
		
		tinymce.init({
			selector: "textarea",
			plugins: [
				 "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
				 "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				 "save table contextmenu directionality emoticons template paste textcolor "
		   ],
		   //image_advtab: true,
		   
		 
		   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image  | print preview  fullpage | forecolor backcolor emoticons" ,
			
			relative_urls: false
		 });

	});

	</script>

<script src='<?php echo URL;?>js/jquery.nestable.js'></script>
<script src="<?php echo URL;?>js/nestable.js"></script>

	<script>
	// document.getElementById('uraian').value='';

	$(document).ready(function()
	{
		
	$('#nestable_list_2').nestable({ 
			group: 1, maxDepth :2
		}).on('change', function() {
	var data = $('.dd').nestable('serialize');
	 ajaxx(data);
	});

	function ajaxx(data){
		  $.ajax({
			type: "POST", 
			 beforeSend: function(msg){
					$("#tunggu").html("tunggu");
			},
			url:'controller/manajemen_halaman_control/manajemen_halaman_control.php?model=manajemen_halaman&method=tambah', 
			data: {
				save: data
			}, 
		   success: function(data){
			 // alert(data);
			 $("#tunggu").html("tunggu");
			 $('#tulis').val(data);
		   }
		});
	}
	
	});
	</script>
	
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>