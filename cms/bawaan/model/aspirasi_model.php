<?php

class aspirasi_model{
	private $db;
	public function __construct($database){
		$this->db = $database;
	}

	public function countCariAspirasi($katacari){
		$katacari = htmlentities(strip_tags($katacari),ENT_QUOTES,'utf-8'); // sanitasi filter
		$katacari = filter_var($katacari, FILTER_SANITIZE_MAGIC_QUOTES);
		
		$query = $this->db->prepare("select * from aspirasi where IFNULL(judul, '') LIKE CONCAT('%', :katacari, '%') OR IFNULL(aspirasi, '') LIKE CONCAT('%', :katacari2, '%')  order by tanggal desc ");
		try{
		$query->bindParam(':katacari',$katacari,PDO::PARAM_STR);
		$query->bindParam(':katacari2',$katacari,PDO::PARAM_STR);
		$query->execute();
		return $query->rowCount();
		}catch(PDOException $e){
			die($e->getMessage());
		}
		}
		
	public function cariAspirasi($katacari,$a,$b){

		$query = $this->db->prepare("select * from aspirasi where IFNULL(judul, '') LIKE CONCAT('%', :katacari, '%') OR IFNULL(aspirasi, '') LIKE CONCAT('%', :katacari2, '%')  order by tanggal desc LIMIT :a,:b");
		$query->bindParam(':katacari',$katacari,PDO::PARAM_STR);
		$query->bindParam(':katacari2',$katacari,PDO::PARAM_STR);
		$query->bindParam(':a',$a,PDO::PARAM_INT);
		$query->bindParam(':b',$b,PDO::PARAM_INT);
		try{
		
			$query->execute();
			
			return $query->fetchAll(PDO::FETCH_ASSOC);
		
		}catch(PDOException $e){
			die($e->getMessage());
		}
		}

	public function getAspirasi($a,$b){

		$query = $this->db->prepare("select * from aspirasi order by id desc limit :a , :b");
		$query->bindParam(':a',$a,PDO::PARAM_INT);
		$query->bindParam(':b',$b,PDO::PARAM_INT);
		try{
			$query->execute();
		}catch(PDOException $e){
			die($e->getMessage());
		}
		return $query->fetchAll(PDO::FETCH_ASSOC);
		}

	public function countAspirasi(){

		$query = $this->db->prepare("select * from aspirasi order by id desc");
			try{
				$query->execute();

				return $query->rowCount();
			}catch(PDOException $e){
				die($e->getMessage());
			}
		}
	
	public function getAspirasiById($id){

		$query = $this->db->prepare("select * from aspirasi where id = :id");
		$query->bindParam('id',$id,PDO::PARAM_INT);
		try{
			$query->execute();
		}catch(PDOException $e){
			die($e->getMessage());
		}
		return $query->fetch(PDO::FETCH_ASSOC);
		}
	
	public function updateAspirasi($blokir,$id){
		
		$query = $this->db->prepare("UPDATE `aspirasi` SET 		blokir = :blokir
																where id = :id
		");
		
		$query->bindParam(':id',$id,PDO::PARAM_INT);
		$query->bindParam(':blokir',$blokir,PDO::PARAM_STR);
		
		
		try{
			$query->execute();
			return true;
		}
		catch(PDOException $e){
		return	die($e->getMessage());
		
		}		
	}
	
	
	public function deleteAspirasi($id){
		if(is_numeric($id)){
			
			$sql="DELETE FROM `aspirasi` WHERE `id` = ?";
			$query = $this->db->prepare($sql);
			$query->bindParam(1, $id,PDO::PARAM_INT);
			
			try{
				$query->execute();
			}
			catch(PDOException $e){
				die($e->getMessage());
			}
		}
	}	

}
?>