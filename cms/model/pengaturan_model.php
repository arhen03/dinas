<?php

class pengaturan_model{
	private $db;
	public function __construct($database){
		$this->db = $database;
	}

	public function getPengaturanById(){

		$query = $this->db->prepare("select * from pengaturan where id	= 1");
		$query->bindParam('id',$id,PDO::PARAM_INT);
		try{
			$query->execute();
		}catch(PDOException $e){
			die($e->getMessage());
		}
		return $query->fetch(PDO::FETCH_ASSOC);
		}
	
	
	public function updatePengumuman($nama,$alamat,$fb,$tw, $lat,$lon,$telp){
		
		$query = $this->db->prepare("UPDATE `pengaturan` SET 	nama 	= :nama,
																alamat 	= :alamat, 
																fb 	= :fb,
																tw 	= :tw,
																lat 	= :lat,
																lon 	= :lon,
																telp 	= :telp
																where id = 1	
		");
		$query->bindParam(':nama',$nama,PDO::PARAM_STR);
		$query->bindParam(':alamat',$alamat,PDO::PARAM_STR);
		$query->bindParam(':fb',$fb,PDO::PARAM_STR);
		$query->bindParam(':tw',$tw,PDO::PARAM_STR);
		$query->bindParam(':lat',$lat);
		$query->bindParam(':lon',$lon);
		$query->bindParam(':telp',$telp);
		try{
			$query->execute();
			return true;
		}
		catch(PDOException $e){
		return	die($e->getMessage());
		
		}		
	}
	
}
?>