<?php 
session_start();
if(isset($_GET['model'])): // for secure
ob_start();
date_default_timezone_set('Asia/Jakarta');
require '../../libs/path.php';
require '../../model/class.php';

$model=$_GET['model'];

$method=$_GET['method'];
$model ;

// echo $method;
// echo $parameter;

if($model=='berita' AND $method=='tambah' ){
	
 	if(isset($_POST['tambah'])){
		$judul = $_POST['judul'];
		$judul = filter_var(strip_tags($judul), FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		$isi = $_POST['isi'];
		// $isi = $libs->stringHtml($isi); // belum butuh sanitasi
		$kategori = $_POST['kategori'];
		$link = $libs->changeLink($judul);
		$penulis = $_SESSION['username'];
		if(isset($_FILES['file']['name'])){

			// $gambar = $libs->uploadImage($_FILES['file']);	// ke folder upload
			$gambar = $libs->uploadImageToFolder('../../../images/content/news/',$_FILES['file']);	//upload
		 	
			$thumbnail = $libs->uploadImageToFolderThumbnail('../../../images/content/news/',$gambar);	//thumbnail
			
			
		}

			 $artikel->insertArtikel($judul,$link,$isi,$gambar,$penulis,$kategori); // method penyimpanan

		echo"<script> alert('Menambah data'); </script>";
		
	}
 
 // return false;
 }

if ($model=='berita' AND $method=='edit' ){
	
	if(isset($_POST['tambah'])){
		$judul = $_POST['judul'];
		$judul = filter_var($judul, FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 

		$id = $_POST['id'];

		$gambar = $_POST['gambar'];
		
		$isi = $_POST['isi'];
		// $isi = filter_var($isi, FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 

		$kategori = $_POST['kategori'];
		
		$penulis = $_SESSION['username'];
			
		$link = $libs->changeLink($judul);
		
		// $isi = $libs->stringHtml($isi);
		
		if(!empty($_FILES['file']['name'])){
			
			// $libs->hapusFile($gambar);
					
			$libs-> hapusGambarSpesific("../../../images/content/news/",$gambar);
			$libs-> hapusGambarSpesific("../../../images/content/news/thumbnails/", $gambar);

			
			$gambar = $libs->uploadImageToFolder('../../../images/content/news/',$_FILES['file']);	//upload
			$thumbnail = $libs->uploadImageToFolderThumbnail('../../../images/content/news/',$gambar);	
			
		
		}
		
		$artikel->updateArtikel($judul,$link,$isi,$gambar,$penulis,$kategori,$id); // method penyimpanan
	}
}	
if($model=='berita' AND $method=='hapus' ){

	$id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
	
	$data = $artikel->getArtikelById($id);
	
	// $libs->hapusFile($data['gambar']);

	$libs-> hapusGambarSpesific("../../../images/content/news/", $data['gambar']);
	$libs-> hapusGambarSpesific("../../../images/content/news/thumbnails/", $data['gambar']);

	
	
	$artikel->deleteArtikel($id);

 echo"<script> alert('Menghapus data'); </script>";
}
 header("location:".URL."berita");
 
 endif;
?>