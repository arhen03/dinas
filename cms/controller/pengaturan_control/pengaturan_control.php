<?php 
if($_REQUEST['model']): // for secure single file
ob_start();
date_default_timezone_set('Asia/Jakarta');


require '../../libs/path.php';
require '../../model/class.php';

$model=$_GET['model'];

$method=$_GET['method'];

if ($model=='pengaturan' AND $method=='edit' ){
	
	if(isset($_POST['tambah'])){
	
		$nama = filter_var($_POST['nama'], FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		$alamat = filter_var($_POST['alamat'], FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		$fb = $_POST['fb'];
		$tw = $_POST['tw'];
		$lat = $_POST['lat'];
		$lon = $_POST['lon'];
		$telp = $_POST['telp'];
	
				
		$pengaturan->updatePengumuman($nama,$alamat,$fb,$tw, $lat,$lon,$telp); // method penyimpanan
	}
	
	header("location:".URL."pengaturan");
}

endif;
?>